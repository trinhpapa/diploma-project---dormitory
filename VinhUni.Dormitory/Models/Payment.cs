﻿using System;

namespace VinhUni.Dormitory.Models
{
   public class Payment
   {
      public int Id { get; set; }

      public int PaymentPeriodId { get; set; }

      public int RenterId { get; set; }

      public decimal DormitoryAmount { get; set; }

      public decimal ElectricAmount { get; set; }

      public decimal WaterAmount { get; set; }

      public decimal Discount { get; set; }

      public decimal TotalAmount { get; set; }

      public short PaymentStatus { get; set; }

      public DateTime PaymentDate { get; set; }

      public virtual PaymentPeriod PaymentPeriod { get; set; }

      public virtual Renter Renter { get; set; }
   }
}