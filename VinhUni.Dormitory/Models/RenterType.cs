﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VinhUni.Dormitory.Models
{
   public class RenterType
   {
      public int Id { get; set; }

      [StringLength(256)]
      public string Name { get; set; }

      public short Discount { get; set; }

      [DefaultValue(true)]
      public bool Status { get; set; } = true;

      [ForeignKey("RenterTypeId")]
      public virtual ICollection<Renter> Renters { get; set; }
   }
}