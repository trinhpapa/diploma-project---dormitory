﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VinhUni.Dormitory.Models
{
   public class Role
   {
      public int Id { get; set; }

      [StringLength(256)]
      public string Name { get; set; }

      public DateTime CreatedDate { get; set; }

      [DefaultValue(true)]
      public bool Status { get; set; } = true;

      [ForeignKey("RoleId")]
      public virtual ICollection<User> Users { get; set; }
   }
}