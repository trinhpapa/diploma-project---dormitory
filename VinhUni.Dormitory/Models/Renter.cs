﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VinhUni.Dormitory.Models {
   public class Renter {
      public int Id { get; set; }

      [StringLength (256)]
      [Display (Name = "Tên")]
      [Required]
      public string Firstname { get; set; }

      [StringLength (256)]
      [Display (Name = "Họ")]
      [Required]
      public string Lastname { get; set; }

      [StringLength (256)]
      [Display (Name = "Số CMND")]
      [Required]
      public string IdentityCard { get; set; }

      [StringLength (256)]
      [Display (Name = "Hộ chiếu")]
      public string Passport { get; set; }

      [StringLength (256)]
      [Display (Name = "Mã sinh viên")]
      public string StudentId { get; set; }

      [StringLength (256)]
      [Display (Name = "Mã cán bộ")]
      public string StaffId { get; set; }

      [Display (Name = "Ngày sinh")]
      public DateTime DateOfBirth { get; set; }

      [StringLength (512)]
      [Display (Name = "Địa chỉ")]
      public string Address { get; set; }

      [StringLength (20)]
      [Display (Name = "Số điện thoại")]
      public string PhoneNumber { get; set; }

      [StringLength (256)]
      [Display (Name = "Email")]
      public string Email { get; set; }

      [Display (Name = "Loại người thuế")]
      public int RenterTypeId { get; set; }

      [DefaultValue (true)]
      [Display (Name = "Trạng thái")]
      public bool Status { get; set; } = true;

      public virtual RenterType RenterType { get; set; }

      [ForeignKey ("RenterId")]
      public virtual ICollection<Contract> Contracts { get; set; }

      [ForeignKey ("RenterId")]
      public virtual ICollection<Payment> Payments { get; set; }
   }
}