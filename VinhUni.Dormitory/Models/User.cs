﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VinhUni.Dormitory.Models
{
   public class User
   {
      public int Id { get; set; }

      [Required(ErrorMessage = "Tên đăng nhập bắt buộc")]
      [StringLength(256, ErrorMessage = "Không được quá 256 ký tự")]
      [MinLength(2, ErrorMessage = "Tối thiểu 2 ký tự")]
      [Display(Name = "Tên đăng nhập")]
      public string Username { get; set; }

      [StringLength(256, ErrorMessage = "Không được quá 256 ký tự")]
      [Display(Name = "Tên")]
      public string Firstname { get; set; }

      [StringLength(256, ErrorMessage = "Không được quá 256 ký tự")]
      [Display(Name = "Họ")]
      public string Lastname { get; set; }

      [NotMapped]
      [StringLength(256, ErrorMessage = "Không được quá 256 ký tự")]
      [MinLength(4, ErrorMessage = "Mật khẩu tối thiểu 2 ký tự")]
      [Display(Name = "Mật khẩu")]
      public string Password { get; set; }

      [StringLength(256)]
      public string PasswordHash { get; set; }

      [StringLength(10)]
      public string PasswordKey { get; set; }

      [Display(Name = "Quyền hạn")]
      public int RoleId { get; set; }

      public DateTime CreatedDate { get; set; }

      [DefaultValue(true)]
      [Display(Name = "Trạng thái")]
      public bool Status { get; set; } = true;

      public virtual Role Role { get; set; }

      [ForeignKey("Creator")]
      public virtual ICollection<Contract> Contracts { get; set; }
   }
}