﻿using System.ComponentModel.DataAnnotations;

namespace VinhUni.Dormitory.Models.NotMapped
{
   public class AccountModel
   {
      public User User { get; set; }

      public ChangePasswordModel ChangePassword { get; set; }
   }

   public class ChangePasswordModel
   {
      [Display(Name = "Mật khẩu cũ")]
      public string OldPassword { get; set; }

      [Display(Name = "Mật khẩu mới")]
      public string Password { get; set; }

      [Display(Name = "Nhập lại mật khẩu mới")]
      public string RePassword { get; set; }
   }
}