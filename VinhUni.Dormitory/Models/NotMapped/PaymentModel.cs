using System.Collections.Generic;

namespace VinhUni.Dormitory.Models.NotMapped
{
    public class PaymentModel
    {
        public Dormitory Dormitory { get; set; } = new Dormitory();
        public Contract Contract { get; set; } = new Contract();
        public PaymentPeriod PaymentPeriod { get; set; } = new PaymentPeriod();
        public List<PaymentPeriod> PaymentPeriods { get; set; } = new List<PaymentPeriod>();
        public List<Payment> Payments { get; set; } = new List<Payment>();
    }
}