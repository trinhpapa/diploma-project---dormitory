﻿using System.ComponentModel.DataAnnotations;

namespace VinhUni.Dormitory.Models.NotMapped
{
   public class LoginModel
   {
      [StringLength(256, ErrorMessage = "Tối đa 256 ký tự")]
      [MinLength(2, ErrorMessage = "Tên đăng nhập tối đa 2 ký tự")]
      [Required(ErrorMessage = "Tên đăng nhập không được để trống")]
      [Display(Name = "Tên đăng nhập")]
      public string Username { get; set; }

      [StringLength(256, ErrorMessage = "Tối đa 256 ký tự")]
      [MinLength(4, ErrorMessage = "Mật khẩu tối thiểu 4 ký tự")]
      [Required(ErrorMessage = "Mật khẩu không được để trống")]
      [Display(Name = "Mật khẩu")]
      public string Password { get; set; }
   }
}