using System.Collections.Generic;

namespace VinhUni.Dormitory.Models.NotMapped {
    public class DormitoryModel {
        public Dormitory Dormitory { get; set; }
        public List<Contract> Contracts { get; set; }
        public List<Contract> Histories { get; set; }
        public List<Payment> PaymentHistories { get; set; }
    }
}