﻿using System.Collections.Generic;
using VinhUni.Dormitory.Models;

namespace VinhUni.Dormitory.Models {
   public class SystemModel {
      public List<Category> DormitoryTypes { get; set; } = new List<Category> ();
      public List<Area> Areas { get; set; } = new List<Area> ();
      public List<RenterType> RenterTypes { get; set; } = new List<RenterType> ();
   }
}