﻿using System;
using System.Collections.Generic;

namespace VinhUni.Dormitory.Models.NotMapped
{
   public class PagedResult<T>
   {
      public int Page { get; set; }

      public int Size { get; set; }

      public int TotalPage => (int)Math.Ceiling((decimal)Total / Size);

      public int Total { get; set; }

      public IList<T> Data { get; set; }
   }
}