﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VinhUni.Dormitory.Models {
   public class Category {
      public int Id { get; set; }

      [StringLength (256)]
      [Display (Name = "Tên")]
      [Required (ErrorMessage = "Không được để trống")]
      public string Name { get; set; }

      [StringLength (256)]
      [Display (Name = "Ký hiệu")]
      [Required (ErrorMessage = "Không được để trống")]
      public string Symbol { get; set; }

      [Display (Name = "Loại")]
      public string Type { get; set; }

      [DefaultValue (true)]
      [Display (Name = "Trạng thái")]
      public bool Status { get; set; } = true;

      [ForeignKey ("DormitoryTypeId")]
      public virtual ICollection<Dormitory> Dormitories { get; set; }
   }
}