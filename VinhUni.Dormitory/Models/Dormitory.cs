﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VinhUni.Dormitory.Models {
   public class Dormitory {
      public int Id { get; set; }

      [StringLength (256)]
      [Display (Name = "Tên phòng")]
      [Required]
      public string Name { get; set; }

      [Display (Name = "Loại phòng")]
      [Required]
      public int DormitoryTypeId { get; set; }

      [Display (Name = "Khu vực")]
      [Required]
      public int AreaId { get; set; }

      [Display (Name = "Số giường")]
      [Required]
      public short Beds { get; set; }

      [Display (Name = "Giá phòng trên tháng")]
      public decimal RentCostPerMonth { get; set; }

      [Display (Name = "Giá điện")]
      public decimal ElectricAmount { get; set; }

      [Display (Name = "Giá nước")]
      public decimal WaterAmount { get; set; }

      [DefaultValue (true)]
      public bool Status { get; set; } = true;

      [NotMapped]
      public int Hired { get; set; }

      public virtual Category DormitoryType { get; set; }
      public virtual Area Area { get; set; }

      [ForeignKey ("DormitoryId")]
      public virtual ICollection<Contract> Contracts { get; set; }

      [ForeignKey ("DormitoryId")]
      public virtual ICollection<PaymentPeriod> PaymentPeriods { get; set; }
   }
}