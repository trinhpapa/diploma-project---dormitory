﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VinhUni.Dormitory.Models {
   public class PaymentPeriod {
      public int Id { get; set; }

      public int DormitoryId { get; set; }

      [StringLength (256)]
      [Display (Name = "Tiêu đề")]
      [Required]
      public string Title { get; set; }

      [Display (Name = "Tháng")]
      [Required]
      public int Month { get; set; }

      [Display (Name = "Năm")]
      [Required]
      public int Year { get; set; }

      [Display (Name = "Tổng tiền phòng")]
      public decimal TotalAmount { get; set; }

      [Display (Name = "Số điện cũ")]
      [Required]
      public int NumberOfElectricityMetersBefore { get; set; }

      [Display (Name = "Số điện mới")]
      [Required]
      public int NumberOfElectricityMeters { get; set; }

      [Display (Name = "Tổng tiền điện")]
      public decimal TotalElectricBill { get; set; }

      [Display (Name = "Số nước cũ")]
      [Required]
      public int NumberOfWaterMetersBefore { get; set; }

      [Display (Name = "Số nước mới")]
      [Required]
      public int NumberOfWaterMeters { get; set; }

      [Display (Name = "Tổng tiền nước")]
      public decimal TotalWaterBill { get; set; }

      [Display (Name = "Tổng số người ở")]
      public int TotalRenter { get; set; }

      [ForeignKey ("PaymentPeriodId")]
      public virtual ICollection<Payment> Payments { get; set; }

      public virtual Dormitory Dormitory { get; set; }
   }
}