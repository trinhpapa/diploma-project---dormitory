﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace VinhUni.Dormitory.Models {
   public class Contract {
      public int Id { get; set; }

      [Display (Name = "KTX")]
      public int DormitoryId { get; set; }

      [Display (Name = "Người thuê")]
      public int RenterId { get; set; }

      [Display (Name = "Từ ngày")]
      public DateTime FromDate { get; set; }

      [Display (Name = "Đến ngày")]
      public DateTime? ToDate { get; set; }

      [Display (Name = "Giảm giá")]
      public decimal Discount { get; set; }

      [Display (Name = "Tiền cọc")]
      public decimal Deposit { get; set; }

      [Display (Name = "Người tạo")]
      public int Creator { get; set; }

      [Display (Name = "Trạng thái")]
      [DefaultValue (true)]
      public bool Status { get; set; } = true;

      public virtual Dormitory Dormitory { get; set; }

      public virtual Renter Renter { get; set; }

      public virtual User User { get; set; }
   }
}