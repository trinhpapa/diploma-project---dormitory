﻿namespace VinhUni.Dormitory
{
   public class Constants
   {
      public static string SessionKey = "user-id";
      public static string SessionRole = "user-role";
      public static string SessionDisplayName = "user-display-name";
   }
}