﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using VinhUni.Dormitory.Helpers;
using VinhUni.Dormitory.Models;
using VinhUni.Dormitory.Models.NotMapped;

namespace VinhUni.Dormitory.Controllers
{
    public class UserController : BaseController
    {
        [LoginRequired("Administrators")]
        public IActionResult Index([FromQuery]string search, [FromQuery]int page = 1, [FromQuery]int size = 15)
        {
            var data = Context.Users.Include(item => item.Role).AsQueryable();

            if (!string.IsNullOrEmpty(search))
            {
                data = data.Where(item => item.Username.Contains(search));
            }

            var total = data.Count();
            data = data.Skip((page - 1) * size).Take(size);

            var result = new PagedResult<User>
            {
                Page = page,
                Size = size,
                Total = total,
                Data = data.ToList()
            };

            return View(result);
        }

        [LoginRequired("Administrators")]
        public IActionResult Create()
        {
            ViewData["Role"] = Context.Roles.Select(a => new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Name
            }).ToList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [LoginRequired("Administrators")]
        public IActionResult Create(User model)
        {
            ViewData["Role"] = Context.Roles.Select(a => new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Name
            }).ToList();

            if (!ModelState.IsValid) return View("Create", model);

            var checkUser = Context.Users.Where(item => item.Username == model.Username);
            if (checkUser.Any())
            {
                TempData["Error"] = "Tên đăng nhập đã tồn tại!";
                return View("Create");
            }

            model.CreatedDate = DateTime.Now;

            Context.Users.Add(model);
            Context.SaveChanges();

            TempData["Success"] = "Thêm thành công!";
            return View("Create");
        }

        [HttpGet("user/{id}/edit")]
        [LoginRequired("Administrators")]
        public IActionResult Edit(int id)
        {
            ViewData["Role"] = Context.Roles.Select(a => new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Name
            }).ToList();

            var user = Context.Users.Find(id);
            if (user == null)
            {
                return RedirectToAction("Index");
            }

            return View(user);
        }

        [HttpPost("user/{id}/edit")]
        [ValidateAntiForgeryToken]
        [LoginRequired("Administrators")]
        public IActionResult Edit(int id, User model)
        {
            ViewData["Role"] = Context.Roles.Select(a => new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Name
            }).ToList();

            if (!ModelState.IsValid) return View("Edit", model);

            var user = Context.Users.Where(item => item.Id == id).AsNoTracking();

            if (!user.Any())
            {
                return RedirectToAction("Index");
            }

            var checkUser = Context.Users.Where(item => item.Username == model.Username).AsNoTracking();
            if (checkUser.Any() && checkUser.FirstOrDefault()?.Id != user.FirstOrDefault()?.Id)
            {
                TempData["Error"] = "Tên đăng nhập đã tồn tại!";
                return View("Edit", model);
            }

            if (!string.IsNullOrEmpty(model.Password))
            {
                var passwordKey = PasswordEncryption.GeneratePasswordKey();
                var passwordHash = PasswordEncryption.EncryptionPasswordWithKey(model.Password, passwordKey);
                model.PasswordHash = passwordHash;
                model.PasswordKey = passwordKey;
            }

            model.CreatedDate = user.FirstOrDefault()?.CreatedDate ?? DateTime.Now;

            Context.Users.Update(model);
            Context.SaveChanges();

            TempData["Success"] = "Cập nhật thành công!";
            return View("Edit", model);
        }

        [HttpGet("user/{id}/delete")]
        public IActionResult Delete(int id)
        {
            ViewData["Role"] = Context.Roles.Select(a => new SelectListItem()
            {
                Value = a.Id.ToString(),
                Text = a.Name
            }).ToList();

            var user = Context.Users.Find(id);

            if (user == null)
            {
                return RedirectToAction("Index");
            }

            if (user.Username == "Admin")
            {
                TempData["Error"] = "Không được xóa tài khoản Admin!";
                return View("Edit", user);
            }

            Context.Users.Remove(user);
            Context.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet("user/{id}/status")]
        public IActionResult ToggleStatus(int id)
        {
            var user = Context.Users.Find(id);
            if (user.Username.ToLower() == "admin") return Ok();
            user.Status = !user.Status;
            Context.SaveChanges();

            return Ok();
        }

        public IActionResult CreateAdmin()
        {
            const string roleName = "Administrators";
            const string username = "Admin";
            const string firstName = "Administrator";
            const string password = "admin";
            const string passwordKey = "QwertyuIop";

            if (!Context.Roles.Any(r => r.Name == roleName))
            {
                var role = new Role
                {
                    Name = roleName,
                    CreatedDate = DateTime.Now
                };
                Context.Roles.Add(role);

                if (!Context.Users.Any(u => u.Username == username))
                {
                    var user = new User
                    {
                        Username = username,
                        Firstname = firstName,
                        PasswordHash = PasswordEncryption.EncryptionPasswordWithKey(password, passwordKey),
                        PasswordKey = passwordKey,
                        RoleId = role.Id,
                        CreatedDate = DateTime.Now
                    };
                    Context.Users.Add(user);
                }
            }

            Context.SaveChanges();

            return Content("Ok");
        }
    }
}