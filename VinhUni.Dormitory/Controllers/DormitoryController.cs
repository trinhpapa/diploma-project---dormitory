using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using VinhUni.Dormitory.Models;
using VinhUni.Dormitory.Models.NotMapped;

namespace VinhUni.Dormitory.Controllers {
    public class DormitoryController : BaseController {

        [LoginRequired]
        [HttpGet ("dormitory/{id}/view")]
        public IActionResult Index (int id) {
            var dormitory = Context.Dormitories.Find (id);
            TempData["Name"] = dormitory.Name;
            TempData["ID"] = id;
            HttpContext.Session.SetInt32 ("dormitory", id);
            var model = new DormitoryModel ();
            model.Dormitory = dormitory;
            model.Contracts = Context.Contracts.Where (item => item.DormitoryId == id && item.Status).Include (item => item.Renter).ToList ();
            model.Histories = Context.Contracts.Where (item => item.DormitoryId == id && !item.Status).Include (item => item.Renter).ToList ();
            model.PaymentHistories = Context.Payments.Where (item => item.PaymentPeriod.DormitoryId == id).Include (item => item.Renter).ToList ();

            return View (model);
        }

        [LoginRequired ("Administrators")]
        public IActionResult Create () {
            ViewData["DormitoryType"] = Context.Categories.Select (a => new SelectListItem () {
                Value = a.Id.ToString (),
                    Text = a.Name
            }).ToList ();
            ViewData["Area"] = Context.Area.Select (a => new SelectListItem () {
                Value = a.Id.ToString (),
                    Text = a.Name
            }).ToList ();
            return View ();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [LoginRequired ("Administrators")]
        public IActionResult Create (Models.Dormitory model) {
            ViewData["DormitoryType"] = Context.Categories.Select (a => new SelectListItem () {
                Value = a.Id.ToString (),
                    Text = a.Name
            }).ToList ();
            ViewData["Area"] = Context.Area.Select (a => new SelectListItem () {
                Value = a.Id.ToString (),
                    Text = a.Name
            }).ToList ();

            if (!ModelState.IsValid) {
                TempData["Error"] = "Dữ liệu không hợp lệ!";
                return View (model);
            }

            var checkName = Context.Dormitories.Any (item => item.Name == model.Name);
            if (checkName) {
                TempData["Error"] = "Tên phòng đã tồn tại!";
                return View (model);
            }

            Context.Dormitories.Add (model);
            Context.SaveChanges ();

            return RedirectToAction ("Index", "Home");
        }

        [HttpGet ("renter/create")]
        [LoginRequired ("Administrators")]
        public IActionResult CreateRenter () {
            ViewData["RenterType"] = Context.RenterTypes.Select (a => new SelectListItem () {
                Value = a.Id.ToString (),
                    Text = a.Name
            }).ToList ();
            return View ();
        }

        [HttpPost ("renter/create")]
        [ValidateAntiForgeryToken]
        [LoginRequired ("Administrators")]
        public IActionResult CreateRenter (Renter model) {
            if (!ModelState.IsValid) {
                TempData["Error"] = "Dữ liệu không hợp lệ!";
                return View (model);
            }

            Context.Renters.Add (model);
            Context.SaveChanges ();
            var dormitoryId = HttpContext.Session.GetInt32 ("dormitory");
            var returnUrl = $"/dormitory/{dormitoryId}/contract";

            return Redirect (returnUrl);
        }

        [HttpGet ("dormitory/{id}/contract")]
        [LoginRequired ("Administrators")]
        public IActionResult CreateContract (int id) {
            TempData["BackUrl"] = $"/dormitory/{id}/view";
            TempData["ID"] = id;
            ViewData["Renters"] = Context.Renters.Select (a => new SelectListItem () {
                Value = a.Id.ToString (),
                    Text = a.Lastname + " " + a.Firstname
            }).ToList ();
            return View ();
        }

        [HttpPost ("dormitory/contract/create")]
        [ValidateAntiForgeryToken]
        [LoginRequired ("Administrators")]
        public IActionResult CreateContract (Contract model) {
            if (!ModelState.IsValid) {
                TempData["Error"] = "Dữ liệu không hợp lệ!";
                ViewData["Renters"] = Context.Renters.Select (a => new SelectListItem () {
                    Value = a.Id.ToString (),
                        Text = a.Lastname + " " + a.Firstname
                }).ToList ();
                return View (model);
            }

            var dormitoryId = HttpContext.Session.GetInt32 ("dormitory");

            var dormitoryCheckBed = Context.Dormitories
                .Where (item => item.Id == dormitoryId)
                .Select (item => item.Beds > item.Contracts.Where (c => c.Status).Count ())
                .FirstOrDefault ();

            if (!dormitoryCheckBed) {
                ViewData["Renters"] = Context.Renters.Select (a => new SelectListItem () {
                    Value = a.Id.ToString (),
                        Text = a.Lastname + " " + a.Firstname
                }).ToList ();
                TempData["Error"] = "Đã hết chỗ trống cho phòng!";
                return View (model);
            }

            model.Creator = (int) HttpContext.Session.GetInt32 (Constants.SessionKey);
            model.DormitoryId = (int) dormitoryId;

            Context.Contracts.Add (model);
            Context.SaveChanges ();

            var returnUrl = $"/dormitory/{dormitoryId}/view";

            return Redirect (returnUrl);
        }

        [HttpGet ("dormitory/{id}/setting")]
        [LoginRequired ("Administrators")]
        public IActionResult Setting (int id) {
            TempData["BackUrl"] = $"/dormitory/{id}/view";
            TempData["ID"] = id;
            return View ();
        }

        [HttpGet ("dormitory/{id}/update")]
        [LoginRequired ("Administrators")]
        public IActionResult Update (Models.Dormitory model) {
            return View ();
        }

        [HttpGet ("dormitory/{id}/createpaymentperiod")]
        [LoginRequired ("Administrators")]
        public IActionResult CreatePaymentPeriod (int id) {
            TempData["BackUrl"] = $"/dormitory/{id}/view";
            TempData["ID"] = id;
            var data = Context.Dormitories.Include (item => item.Contracts).FirstOrDefault (item => item.Id == id);
            var result = new PaymentModel ();
            result.Dormitory = data;
            return View (result);
        }

        [HttpPost ("dormitory/{dormitoryId}/createpaymentperiod")]
        [LoginRequired ("Administrators")]
        public IActionResult CreatePaymentPeriod (int dormitoryId, PaymentPeriod model) {
            TempData["BackUrl"] = $"/dormitory/{dormitoryId}/view";
            TempData["DormitoryId"] = dormitoryId;
            if (!ModelState.IsValid) {
                TempData["Error"] = "Dữ liệu không chính xác!";
                var data1 = Context.Dormitories.Include (item => item.Contracts).FirstOrDefault (item => item.Id == dormitoryId);
                var result1 = new PaymentModel ();
                result1.Dormitory = data1;
                return View (result1);
            }

            var checkExist = Context.PaymentPeriods.Any (item => item.DormitoryId == dormitoryId && item.Month == model.Month && item.Year == item.Year);

            if (checkExist) {
                TempData["Error"] = "Đã tồn tại đợt thanh toán của tháng này!";
                var data1 = Context.Dormitories.Include (item => item.Contracts).FirstOrDefault (item => item.Id == dormitoryId);
                var result1 = new PaymentModel ();
                result1.Dormitory = data1;
                return View (result1);
            }

            var dormitory = Context.Dormitories.Find (dormitoryId);

            model.DormitoryId = dormitoryId;
            model.TotalAmount = dormitory.RentCostPerMonth;
            model.TotalElectricBill = (model.NumberOfElectricityMeters - model.NumberOfElectricityMetersBefore) * dormitory.ElectricAmount;
            model.TotalWaterBill = (model.NumberOfWaterMeters - model.NumberOfWaterMetersBefore) * dormitory.WaterAmount;
            model.TotalRenter = Context.Contracts
                .Count (item => item.DormitoryId == dormitoryId && !item.Status);

            Context.PaymentPeriods.Add (model);
            Context.SaveChanges ();

            return Redirect ($"/dormitory/{dormitoryId}/view");
        }

        [HttpGet ("dormitory/{dormitoryId}/checkout/{contractId}")]
        [LoginRequired ("Administrators")]
        public IActionResult CheckOut (int dormitoryId, int contractId) {
            TempData["DormitoryId"] = dormitoryId;
            var contract = Context.Contracts.Include (item => item.Renter).FirstOrDefault (item => item.Id == contractId);
            return View (contract);
        }

        [HttpGet ("dormitory/{dormitoryId}/checkout/{contractId}/submit")]
        [LoginRequired ("Administrators")]
        public IActionResult CheckOutSubmit (int dormitoryId, int contractId) {
            var contract = Context.Contracts.FirstOrDefault (item => item.Id == contractId);
            contract.Status = false;
            contract.ToDate = DateTime.Now;
            Context.SaveChanges ();
            return Redirect ($"/dormitory/{dormitoryId}/view");
        }

        [HttpGet ("dormitory/{dormitoryId}/payment/{contractId}")]
        [LoginRequired ("Administrators")]
        public IActionResult Payment (int dormitoryId, int contractId) {
            TempData["DormitoryId"] = dormitoryId;

            var contract = Context.Contracts.FirstOrDefault (item => item.Id == contractId && item.Status);
            var dormitory = Context.Dormitories.Find (dormitoryId);
            var paymentPeriods = Context.PaymentPeriods.Where (item => item.DormitoryId == dormitoryId && item.Month >= contract.FromDate.Month && item.Year >= contract.FromDate.Year).OrderByDescending (item => item.Year).ThenByDescending (item => item.Month).ToList ();

            var result = new PaymentModel ();
            result.Contract = contract;
            result.Dormitory = dormitory;
            result.PaymentPeriods = paymentPeriods;

            return View (result);
        }

        [HttpGet ("dormitory/{dormitoryId}/payment/{contractId}/submit")]
        [LoginRequired ("Administrators")]
        public IActionResult PaymentSubmit (int dormitoryId, int contractId) {
            TempData["DormitoryId"] = dormitoryId;

            var contract = Context.Contracts.FirstOrDefault (item => item.Id == contractId && item.Status);
            var dormitory = Context.Dormitories.Find (dormitoryId);
            var paymentPeriods = Context.PaymentPeriods.Where (item => item.DormitoryId == dormitoryId && item.Month >= contract.FromDate.Month && item.Year >= contract.FromDate.Year).OrderByDescending (item => item.Year).ThenByDescending (item => item.Month).ToList ();

            var result = new PaymentModel ();
            result.Contract = contract;
            result.Dormitory = dormitory;
            result.PaymentPeriods = paymentPeriods;
            var url = $"/dormitory/{dormitoryId}/payment/{contractId}";
            return Redirect (url);
        }
    }
}