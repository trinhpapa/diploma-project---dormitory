﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace VinhUni.Dormitory.Controllers {
   public class HomeController : BaseController {
      [LoginRequired]
      public IActionResult Index (string search, int area, int dormitoryType, bool? status) {
         var query = Context.Dormitories.AsQueryable ();
         if (!string.IsNullOrEmpty (search)) {
            query = query.Where (item => item.Name.Contains (search) || item.Contracts.Any (c => c.Renter.Firstname.Contains (search) || c.Renter.Firstname.Contains (search)));
         }
         if (area != 0) {
            query = query.Where (item => item.AreaId == area);
         }
         if (dormitoryType != 0) {
            query = query.Where (item => item.DormitoryTypeId == dormitoryType);
         }
         if (status != null) {
            query = query.Where (item => item.Status == status);
         }
         var data = query.Select (item => new Models.Dormitory () {
            Id = item.Id,
               Name = item.Name,
               Area = item.Area,
               Beds = item.Beds,
               Status = item.Status,
               Hired = item.Contracts.Count (c => c.Status)
         }).OrderBy (item => item.Name).ToList ();

         ViewData["Area"] = Context.Area.Select (a => new SelectListItem () {
            Value = a.Id.ToString (),
               Text = a.Name
         }).ToList ();

         ViewData["DormitoryType"] = Context.Categories.Select (a => new SelectListItem () {
            Value = a.Id.ToString (),
               Text = a.Name
         }).ToList ();

         return View (data);
      }

      public IActionResult CreatePaymentPeriod () {
         return View ();
      }

      public IActionResult AccessDenied () {
         return View ();
      }
   }
}