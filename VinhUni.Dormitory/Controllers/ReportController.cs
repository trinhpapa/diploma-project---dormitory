using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace VinhUni.Dormitory.Controllers {
    public class ReportController : BaseController {
        public IActionResult Index (string type) {
            if (type == "renter_debt") {
                var data = Context.Renters.Where (item => item.Contracts.Any ()).ToList ();
                return View (data);
            }
            return View ();
        }
    }
}