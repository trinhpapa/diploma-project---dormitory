﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using VinhUni.Dormitory.Models;

namespace VinhUni.Dormitory.Controllers {
   public class SystemController : BaseController {
      public IActionResult Index () {
         var model = new SystemModel ();
         model.DormitoryTypes = Context.Categories.ToList ();
         model.Areas = Context.Area.ToList ();
         model.RenterTypes = Context.RenterTypes.ToList ();
         return View (model);
      }

      public IActionResult CreateDormitoryType () {
         return View ();
      }

      [HttpPost]
      public IActionResult CreateDormitoryType (Category model) {
         if (!ModelState.IsValid) {
            TempData["Error"] = "Dữ liệu không hợp lệ!";
            return View (model);
         }

         var checkName = Context.Categories.Any (item => item.Symbol == model.Symbol && item.Type == model.Type);
         if (checkName) {
            TempData["Error"] = "Ký hiệu loại phòng đã tồn tại!";
            return View (model);
         }

         model.Type = "DormitoryType";

         Context.Categories.Add (model);
         Context.SaveChanges ();

         TempData["Success"] = "Thêm thành công!";
         return View (model);
      }

      [LoginRequired]
      [HttpGet ("dormitorytype/{id}/edit")]
      public IActionResult EditDormitoryType (int id) {
         var item = Context.Categories.Find (id);
         return View (item);
      }

      [LoginRequired]
      [HttpPost ("dormitorytype/{id}/edit")]
      public IActionResult EditDormitoryType (int id, Category model) {
         if (!ModelState.IsValid) {
            TempData["Error"] = "Dữ liệu không hợp lệ!";
            return View (model);
         }

         var checkName = Context.Categories.Any (item => item.Symbol == model.Symbol && item.Type == model.Type && item.Id != model.Id);
         if (checkName) {
            TempData["Error"] = "Ký hiệu loại phòng đã tồn tại!";
            return View (model);
         }

         Context.Categories.Update (model);
         Context.SaveChanges ();

         TempData["Success"] = "Chỉnh sửa thành công!";
         return View (model);
      }

      [LoginRequired]
      [HttpGet ("dormitorytype/{id}/delete")]
      public IActionResult DeleteDormitoryType (int id) {
         var item = Context.Categories.Find (id);
         if (item == null) {
            TempData["Error"] = "Không tồn tại!";
            return View ("EditDormitoryType", item);
         }
         if (item.Dormitories != null) {
            TempData["Error"] = "Đã được sử dụng, không thể xóa!";
            return View ("EditDormitoryType", item);
         }

         Context.Categories.Remove (item);

         Context.SaveChanges ();

         TempData["Success"] = "Xóa thành công!";
         return View ("EditDormitoryType", item);
      }

      public IActionResult CreateArea () {
         return View ();
      }

      [HttpPost]
      public IActionResult CreateArea (Area model) {
         if (!ModelState.IsValid) {
            TempData["Error"] = "Dữ liệu không hợp lệ!";
            return View (model);
         }

         var checkName = Context.Area.Any (item => item.Symbol == model.Symbol && item.Type == model.Type);
         if (checkName) {
            TempData["Error"] = "Ký hiệu loại phòng đã tồn tại!";
            return View (model);
         }

         model.Type = "Area";

         Context.Area.Add (model);
         Context.SaveChanges ();

         TempData["Success"] = "Thêm thành công!";
         return View (model);
      }

      [LoginRequired]
      [HttpGet ("area/{id}/edit")]
      public IActionResult EditArea (int id) {
         var item = Context.Area.Find (id);
         return View (item);
      }

      [LoginRequired]
      [HttpPost ("area/{id}/edit")]
      public IActionResult EditArea (int id, Area model) {
         if (!ModelState.IsValid) {
            TempData["Error"] = "Dữ liệu không hợp lệ!";
            return View (model);
         }

         var checkName = Context.Area.Any (item => item.Symbol == model.Symbol && item.Type == model.Type && item.Id != model.Id);
         if (checkName) {
            TempData["Error"] = "Ký hiệu loại phòng đã tồn tại!";
            return View (model);
         }

         Context.Area.Update (model);
         Context.SaveChanges ();

         TempData["Success"] = "Chỉnh sửa thành công!";
         return View (model);
      }

      [LoginRequired]
      [HttpGet ("area/{id}/delete")]
      public IActionResult DeleteArea (int id) {
         var item = Context.Area.Find (id);
         if (item == null) {
            TempData["Error"] = "Không tồn tại!";
            return View ("EditArea", item);
         }
         if (item.Dormitories != null) {
            TempData["Error"] = "Đã được sử dụng, không thể xóa!";
            return View ("EditArea", item);
         }

         Context.Area.Remove (item);

         Context.SaveChanges ();

         TempData["Success"] = "Xóa thành công!";
         return View ("EditArea", item);
      }

      public IActionResult CreateRenterType () {
         return View ();
      }

      [HttpPost]
      public IActionResult CreateRenterType (RenterType model) {
         if (!ModelState.IsValid) {
            TempData["Error"] = "Dữ liệu không hợp lệ!";
            return View (model);
         }

         var checkName = Context.RenterTypes.Any (item => item.Name == model.Name);
         if (checkName) {
            TempData["Error"] = "Tên đã tồn tại!";
            return View (model);
         }

         Context.RenterTypes.Add (model);
         Context.SaveChanges ();

         TempData["Success"] = "Thêm thành công!";
         return View (model);
      }

      [LoginRequired]
      [HttpGet ("rentertype/{id}/edit")]
      public IActionResult EditRenterType (int id) {
         var item = Context.RenterTypes.Find (id);
         return View (item);
      }

      [LoginRequired]
      [HttpPost ("rentertype/{id}/edit")]
      public IActionResult EditRenterType (int id, RenterType model) {
         if (!ModelState.IsValid) {
            TempData["Error"] = "Dữ liệu không hợp lệ!";
            return View (model);
         }

         var checkName = Context.RenterTypes.Any (item => item.Name == model.Name && item.Id != model.Id);
         if (checkName) {
            TempData["Error"] = "Tên đã tồn tại!";
            return View (model);
         }

         Context.RenterTypes.Update (model);
         Context.SaveChanges ();

         TempData["Success"] = "Chỉnh sửa thành công!";
         return View (model);
      }

      [LoginRequired]
      [HttpGet ("rentertype/{id}/delete")]
      public IActionResult DeleteRenterType (int id) {
         var item = Context.RenterTypes.Find (id);
         if (item == null) {
            TempData["Error"] = "Không tồn tại!";
            return View ("EditArea", item);
         }
         if (item.Renters != null) {
            TempData["Error"] = "Đã được sử dụng, không thể xóa!";
            return View ("EditArea", item);
         }

         Context.RenterTypes.Remove (item);

         Context.SaveChanges ();

         TempData["Success"] = "Xóa thành công!";
         return View ("EditArea", item);
      }
   }
}