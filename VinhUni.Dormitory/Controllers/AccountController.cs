﻿using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VinhUni.Dormitory.Helpers;
using VinhUni.Dormitory.Models;
using VinhUni.Dormitory.Models.NotMapped;

namespace VinhUni.Dormitory.Controllers {
   public class AccountController : BaseController {
      [LoginRequired]
      public IActionResult Index () {
         var userId = HttpContext.Session.GetInt32 (Constants.SessionKey);
         var user = Context.Users.Find (userId);

         var model = new AccountModel {
            User = user,
            ChangePassword = new ChangePasswordModel ()
         };

         return View (model);
      }

      public IActionResult Login () {
         return View ();
      }

      [HttpPost]
      [ValidateAntiForgeryToken]
      public IActionResult Login ([FromForm] LoginModel model) {
         if (!ModelState.IsValid) return View ("Login", model);
         var user = Context.Users.AsNoTracking ().FirstOrDefault (u => u.Username == model.Username);
         if (user == null) {
            TempData["ErrorMessage"] = "Tài khoản hoặc mật khẩu không đúng";
            return View ("Login");
         }

         if (!user.Status) {
            TempData["ErrorMessage"] = "Tài khoản đã bị khóa";
            return View ("Login");
         }

         var passwordHash = PasswordEncryption.EncryptionPasswordWithKey (model.Password, user.PasswordKey);
         if (user.PasswordHash != passwordHash) {
            TempData["ErrorMessage"] = "Tài khoản hoặc mật khẩu không đúng";
            return View ("Login");
         }

         var userRole = Context.Roles.Find (user.RoleId);
         HttpContext.Session.SetInt32 (Constants.SessionKey, user.Id);
         HttpContext.Session.SetString (Constants.SessionDisplayName, user.Firstname + " " + user.Lastname);
         HttpContext.Session.SetString (Constants.SessionRole, userRole.Name);
         return Redirect ("/home");
      }

      [LoginRequired]
      public IActionResult SignOut () {
         HttpContext.Session.Clear ();
         return Redirect ("/login");
      }

      public IActionResult ForgotPassword () {
         return View ();
      }

      [LoginRequired]
      public IActionResult ChangeInformation (User model) {
         var userId = HttpContext.Session.GetInt32 (Constants.SessionKey);
         var user = Context.Users.Find (userId);
         user.Firstname = model.Firstname;
         user.Lastname = model.Lastname;
         Context.SaveChanges ();
         TempData["ResultInformation"] = "Cập nhật thành công";

         var result = new AccountModel {
            User = user,
            ChangePassword = new ChangePasswordModel ()
         };

         return View ("Index", result);
      }

      [LoginRequired]
      public IActionResult ChangePassword (ChangePasswordModel model) {
         var userId = HttpContext.Session.GetInt32 (Constants.SessionKey);
         var user = Context.Users.Find (userId);

         if (model.RePassword != model.Password) {
            TempData["Error"] = "Mật khẩu mới không trùng khớp";
            var resultError = new AccountModel {
               User = user,
               ChangePassword = model
            };
            return View ("Index", resultError);
         }

         var passwordOld = PasswordEncryption.EncryptionPasswordWithKey (model.OldPassword, user.PasswordKey);

         if (passwordOld != user.PasswordHash) {
            TempData["Error"] = "Mật khẩu cũ không đúng";
            var resultError = new AccountModel {
               User = user,
               ChangePassword = model
            };
            return View ("Index", resultError);
         }

         var passwordKey = PasswordEncryption.GeneratePasswordKey ();

         user.PasswordHash = PasswordEncryption.EncryptionPasswordWithKey (model.Password, passwordKey);
         user.PasswordKey = passwordKey;

         Context.SaveChanges ();

         TempData["Result"] = "Cập nhật thành công";

         var result = new AccountModel {
            User = user,
            ChangePassword = new ChangePasswordModel ()
         };

         return View ("Index", result);
      }
   }
}