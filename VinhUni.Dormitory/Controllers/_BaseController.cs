﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using VinhUni.Dormitory.Data;

namespace VinhUni.Dormitory.Controllers
{
   public class BaseController : Controller
   {
      protected ApplicationDbContext Context => (ApplicationDbContext)HttpContext.RequestServices.GetService(typeof(ApplicationDbContext));
   }
}