﻿using Microsoft.EntityFrameworkCore;
using VinhUni.Dormitory.Models;

namespace VinhUni.Dormitory.Data {
   public class ApplicationDbContext : DbContext {
      public ApplicationDbContext (DbContextOptions options) : base (options) { }

      public DbSet<User> Users { get; set; }
      public DbSet<Role> Roles { get; set; }
      public DbSet<Models.Dormitory> Dormitories { get; set; }
      public DbSet<Category> Categories { get; set; }
      public DbSet<Area> Area { get; set; }
      public DbSet<Renter> Renters { get; set; }
      public DbSet<RenterType> RenterTypes { get; set; }
      public DbSet<Contract> Contracts { get; set; }
      public DbSet<PaymentPeriod> PaymentPeriods { get; set; }
      public DbSet<Payment> Payments { get; set; }
   }
}