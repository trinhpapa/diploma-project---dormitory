﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using VinhUni.Dormitory.Data;

namespace VinhUni.Dormitory
{
   public class Startup
   {
      public Startup(IConfiguration configuration)
      {
         Configuration = configuration;
      }

      public IConfiguration Configuration { get; }

      public void ConfigureServices(IServiceCollection services)
      {
         services.AddSession(options =>
         {
            options.IdleTimeout = TimeSpan.FromHours(4);
            options.Cookie.HttpOnly = true;
         });

         services.AddDbContext<ApplicationDbContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

         services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
      }

      public void Configure(IApplicationBuilder app, IHostingEnvironment env)
      {
         if (env.IsDevelopment())
         {
            app.UseDeveloperExceptionPage();
         }
         else
         {
            app.UseExceptionHandler("/Home/Error");
         }

         app.UseStaticFiles();

         app.UseSession();

         app.UseMvc(routes =>
         {
            routes.MapRoute(
               "Login",
               "login",
               new { controller = "Account", action = "Login" }
            );
            routes.MapRoute(
               "Forgot Password",
               "forgot-password",
               new { controller = "Account", action = "ForgotPassword" }
            );
            routes.MapRoute(
               "Account",
               "account",
               new { controller = "Account", action = "Index" }
            );
            routes.MapRoute(
               "User",
               "user",
               new { controller = "User", action = "Index" }
            );
            routes.MapRoute(
               "Access Denied",
               "access-denied",
               new { controller = "Home", action = "AccessDenied" }
            );
            routes.MapRoute(
                   name: "default",
                   template: "{controller=Home}/{action=Index}/{id?}");
         });
      }
   }
}