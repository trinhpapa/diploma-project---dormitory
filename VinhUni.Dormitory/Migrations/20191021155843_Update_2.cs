﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VinhUni.Dormitory.Migrations
{
    public partial class Update_2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dormitories_DormitoryTypes_DormitoryId",
                table: "Dormitories");

            migrationBuilder.DropIndex(
                name: "IX_Dormitories_DormitoryId",
                table: "Dormitories");

            migrationBuilder.DropColumn(
                name: "DormitoryId",
                table: "Dormitories");

            migrationBuilder.AddColumn<int>(
                name: "DormitoryTypeId",
                table: "Dormitories",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Dormitories_DormitoryTypeId",
                table: "Dormitories",
                column: "DormitoryTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Dormitories_DormitoryTypes_DormitoryTypeId",
                table: "Dormitories",
                column: "DormitoryTypeId",
                principalTable: "DormitoryTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dormitories_DormitoryTypes_DormitoryTypeId",
                table: "Dormitories");

            migrationBuilder.DropIndex(
                name: "IX_Dormitories_DormitoryTypeId",
                table: "Dormitories");

            migrationBuilder.DropColumn(
                name: "DormitoryTypeId",
                table: "Dormitories");

            migrationBuilder.AddColumn<int>(
                name: "DormitoryId",
                table: "Dormitories",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Dormitories_DormitoryId",
                table: "Dormitories",
                column: "DormitoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Dormitories_DormitoryTypes_DormitoryId",
                table: "Dormitories",
                column: "DormitoryId",
                principalTable: "DormitoryTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
