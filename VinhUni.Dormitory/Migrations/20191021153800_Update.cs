﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VinhUni.Dormitory.Migrations
{
    public partial class Update : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dormitories_DormitoryTypes_DormitoryId",
                table: "Dormitories");

            migrationBuilder.AlterColumn<int>(
                name: "DormitoryId",
                table: "Dormitories",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Dormitories_DormitoryTypes_DormitoryId",
                table: "Dormitories",
                column: "DormitoryId",
                principalTable: "DormitoryTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dormitories_DormitoryTypes_DormitoryId",
                table: "Dormitories");

            migrationBuilder.AlterColumn<int>(
                name: "DormitoryId",
                table: "Dormitories",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Dormitories_DormitoryTypes_DormitoryId",
                table: "Dormitories",
                column: "DormitoryId",
                principalTable: "DormitoryTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
