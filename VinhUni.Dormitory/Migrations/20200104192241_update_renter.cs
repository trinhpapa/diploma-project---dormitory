﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VinhUni.Dormitory.Migrations
{
    public partial class update_renter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "StaffId",
                table: "Renters",
                maxLength: 256,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StaffId",
                table: "Renters");
        }
    }
}
