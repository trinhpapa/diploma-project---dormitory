﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VinhUni.Dormitory.Migrations
{
    public partial class add_area : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Area",
                table: "Dormitories");

            migrationBuilder.AddColumn<int>(
                name: "AreaId",
                table: "Dormitories",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Area",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 256, nullable: false),
                    Symbol = table.Column<string>(maxLength: 256, nullable: false),
                    Type = table.Column<string>(nullable: true),
                    Status = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Area", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Dormitories_AreaId",
                table: "Dormitories",
                column: "AreaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Dormitories_Area_AreaId",
                table: "Dormitories",
                column: "AreaId",
                principalTable: "Area",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dormitories_Area_AreaId",
                table: "Dormitories");

            migrationBuilder.DropTable(
                name: "Area");

            migrationBuilder.DropIndex(
                name: "IX_Dormitories_AreaId",
                table: "Dormitories");

            migrationBuilder.DropColumn(
                name: "AreaId",
                table: "Dormitories");

            migrationBuilder.AddColumn<string>(
                name: "Area",
                table: "Dormitories",
                nullable: true);
        }
    }
}
